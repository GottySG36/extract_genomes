// Utility to extract specific fasta sequences from a given list / file

package main

import (
    "bufio"
    "flag"
    "fmt"
    "io"
    "log"
    "os"
    "strings"
    "sync"
)

var (
    fnas = flag.String("fnas", "", "Input fasta containing the sequences to look for")
    repm = flag.String("rep-meta", "", "Kraken output used to map ids to fasta sequences")
    ids  = flag.String("ids", "", "List of ids to look for")
    fIds = flag.String("f-ids", "", "File containing a list of ids to look for.")
    outp = flag.String("o", "", "Output directories, comma seperated, in which to extract the most abundant taxids' genomes")
    pref = flag.String("p", "sample", "Prefix used to build output file names, has to be a comma seperated list if multiple input files")
    jobs = flag.Int("t", 1, "Number of concurrent jobs to run for writing output")
)

type fasta struct {
    Header      string
    Sequence    string
}

type Genome []*fasta

type Metagenome map[string]*Genome

// Reads from the kraken output and builds a map where the keys are
// the fasta headers and the values are the associated ids
// Return an error if the file can't be opened
func LoadRepMeta(fi string, ids map[string]bool) (map[string]string, error) {
    defer func(){
        if p := recover(); p != nil {
            panic(fmt.Errorf("An unexpected error occured : %v\n", p))
        }
    }()

    var b strings.Builder
    head2Id := make(map[string]string)
    f, err  := os.Open(fi)
    defer f.Close()
    if err != nil {
        return nil, err
    }
    r := bufio.NewReader(f)
    for {
        lr, more, err := r.ReadLine()
        if err == io.EOF {
            break
        } else if len(lr) == 0 {
            continue
        } else if err != nil {
            return nil, err
        }
        b.Write(lr)
        if more { continue }
        line := strings.Split(b.String(), "\t")
        if ids[ line[2] ] {
            head2Id[line[1]] = line[2]
        }
        b.Reset()
    }
    return head2Id, err
}

func ExtractGenomes(fna string, m map[string]string, ids map[string]bool) (Metagenome, error) {
    defer func(){
        if p := recover(); p != nil {
            panic(fmt.Errorf("An unexpected error occured : %v\n", p))
        }
    }()
    f, err := os.Open(fna)
    defer f.Close()
    if err != nil {
        return nil, err
    }

    first := true
    var b strings.Builder
    meta := make(Metagenome)
    for id, _ := range ids {
        meta[id] = &Genome{}
    }
    var seq = &fasta{}

    r := bufio.NewReader(f)
    for {
        lr, _, err := r.ReadLine()
        if err == io.EOF {
            seq.Sequence = b.String()
            if m[seq.Header] != "" {
                *(meta[m[seq.Header]]) = append(*(meta[m[seq.Header]]), seq)
            }
            break
        } else if len(lr) == 0 {
            continue
        } else if err != nil {
            return nil, err
        }
        if lr[0] == '>' || lr[0] == '@' {
            if !first {
                seq.Sequence = b.String()
                if m[seq.Header] != "" {
                    *(meta[m[seq.Header]]) = append(*(meta[m[seq.Header]]), seq)
                }
                b.Reset()
            }
            first = false
            seq = &fasta{Header:string(lr[1:len(lr)])}
        } else {
            b.Write(lr)
        }
    }
    return meta, nil
}

func (f *fasta) String() string {
    return fmt.Sprintf(">%v\n%v", f.Header, f.Sequence)
}

func (g *Genome) String() string {
    var b strings.Builder
    if len(*g) < 1 { return "" }

    for _, f := range (*g)[:len(*g)-1] {
        b.WriteString(f.String())
        b.WriteString("\n")
    }
    b.WriteString((*g)[len(*g)-1].String())
    return b.String()
}

func (m Metagenome) Write(o, p string, t int) (err error) {
    defer func() {
        if r := recover(); r != nil {
            err = fmt.Errorf("%v", r)
        }
        return
    }()

    threads := make(chan struct{}, t)
    var wg sync.WaitGroup
    for id, gen := range m {
        wg.Add(1)
        go func(o, p, i string, g *Genome) {
            defer wg.Done()
            threads <- struct{}{}
            outp := fmt.Sprintf("%v/%v_%v.fasta", o, p, i)
            f, err := os.Create(outp)
            defer f.Close()
            if err != nil {
                panic(err)
            }

            _, err = f.WriteString(g.String())
            if err != nil {
                panic(err)
            }
            <-threads
        }(o, p, id, gen)
    }
    wg.Wait()
    return
}

func main() {
    flag.Parse()

    l_ids := strings.Split(*ids, ",")
    m_ids := make(map[string]bool)
    for _, val := range l_ids {
        m_ids[val] = true
    }

    rm, err := LoadRepMeta(*repm, m_ids)
    if err != nil {
        log.Fatalf("Error -:- LoadRepMeta : %v\n", err)
    }

    meta, err := ExtractGenomes(*fnas, rm, m_ids)
    if err != nil {
        log.Fatalf("Error -:- ExtractGenomes : %v\n", err)
    }

    err = meta.Write(*outp, *pref, *jobs)
    if err != nil {
        log.Fatalf("Error -:- Write :%v\n", err)
    }
}
